# Sphinx Search Upstart README

This is a simple upstart job to start/stop the [Sphinx Search] [1] `searchd` on
boot or with `start`, `stop` and `restart`.

It has been tested with Sphinx 0.9.9 on Ubuntu 10.04, compiled manually (later
versions of Sphinx have their own Ubuntu binaries I imagine having their ow
start/stop scripts).


## Installation

    $ sudo cp searchd.conf /etc/init

If there is no lib directory e.g. fresh install, that needs to be made 
manually and owned by `SEARCHD_USER`:

    $ sudo mkdir /var/lib/sphinx
    $ sudo chown www-data:www-data /var/lib/sphinx


## Usage

    $ sudo start searchd


## Configuration

- `SEARCHD_USER` Default: www-data

   By default it is set up to run `searchd` under the `www-data` user id. This
   allows shelling out to run `searchd` and `indexer` commands from PHP scripts
   etc.


- `SEARCHD_BIN` Default: /usr/local/bin/searchd

   Path to searchd binary, default is `/usr/lib/bin/searchd`.


- `SEARCHD_CONF` Default: /usr/local/etc/sphinx/searchd.conf

   File containing general searchd config section of Sphinx config, see
   `SPHINX_INDEXES` for specific indexes.


- `SPHINX_INDEXES` Default: /usr/local/etc/sphinx/indexes

   Directory containing Sphinx index definitions. By convention I have one
   index per file, with optionally a delta index. On start (and restart) all
   the files in this directory will be concatenated together with
   `SEARCHD_CONF` giving a complete config file.


- `SPHINX_CONF` Default: /usr/local/etc/sphinx/sphinx.conf

   Path to config - you don't actually create this file, see `SEARCHD_CONF` and
   `SPHINX_INDEXES`, so should leave it alone!


- `SPHINX_RUN` Default: /var/run/sphinx

   Directory to store PID file, must match settings in `SEARCHD_CONF`.


- `SPHINX_LOG` Default: /var/log/sphinx

   Directory to write Sphinx logs, must match settings in `SEARCHD_CONF`.


[1]:http://sphinxsearch.com
